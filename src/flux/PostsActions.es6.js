import { Actions} from 'flummox';

export class PostsActions extends Actions {
  filterPosts(filter) {
    return {filter : filter};
  }
}