webpackHotUpdate(0,{

/***/ 201:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _moment = __webpack_require__(202);

	var _moment2 = _interopRequireDefault(_moment);

	var _classnames = __webpack_require__(159);

	var _classnames2 = _interopRequireDefault(_classnames);

	var _Comments = __webpack_require__(287);

	var _Comments2 = _interopRequireDefault(_Comments);

	var Post = (function (_React$Component) {
	  function Post() {
	    _classCallCheck(this, Post);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Post, _React$Component);

	  _createClass(Post, [{
	    key: 'render',
	    value: function render() {
	      var classes = (0, _classnames2['default'])('post', this.props.className, {
	        'post-no-media': !!this.props.image
	      });
	      var variant = this.props.variant;

	      if (this.props.image) {
	        var imageComponentClasses = (0, _classnames2['default'])('post-media', 'post-media-' + variant);
	        var href = variant !== 'modal' ? '#/posts/' + this.props.type + 's/' + this.props.id : '#';

	        var imageComponent = _react2['default'].createElement(
	          'div',
	          { className: imageComponentClasses },
	          _react2['default'].createElement(
	            'a',
	            { href: href },
	            _react2['default'].createElement('img', { src: this.props.image.md })
	          ),
	          this.props.type === 'video' && _react2['default'].createElement(
	            'div',
	            { className: 'post-video-indicator' },
	            _react2['default'].createElement('i', { className: 'fa fa-play-circle-o' })
	          )
	        );

	        var normalPostImage = variant === 'standard' && imageComponent;
	        var modalPostImage = variant === 'modal' && imageComponent;
	      }

	      return _react2['default'].createElement(
	        'div',
	        { className: classes },
	        modalPostImage,
	        _react2['default'].createElement(
	          'div',
	          { className: 'post-wrap' },
	          _react2['default'].createElement(
	            'div',
	            { className: 'post-username' },
	            this.props.username
	          ),
	          _react2['default'].createElement(
	            'div',
	            { className: 'post-message' },
	            this.props.message
	          ),
	          _react2['default'].createElement('img', {
	            src: this.props.avatar,
	            className: 'post-avatar',
	            height: '40',
	            width: '40' })
	        ),
	        _react2['default'].createElement(
	          'ul',
	          { className: 'post-meta' },
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('i', { className: 'fa fa-heart-o' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('i', { className: 'fa fa-reply' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            (0, _moment2['default'])(this.props.createDate).fromNow(true)
	          )
	        ),
	        normalPostImage,
	        !_lodash2['default'].isEmpty(this.props.comments) && _react2['default'].createElement(_Comments2['default'], { comments: this.props.comments })
	      );
	    }
	  }], [{
	    key: 'propTypes',
	    get: function () {
	      return {
	        username: _react2['default'].PropTypes.string,
	        message: _react2['default'].PropTypes.string,
	        image: _react2['default'].PropTypes.object,
	        avatar: _react2['default'].PropTypes.string,
	        createDate: _react2['default'].PropTypes.string,
	        comments: _react2['default'].PropTypes.array,
	        variant: _react2['default'].PropTypes.string
	      };
	    }
	  }, {
	    key: 'defaultProps',
	    get: function () {
	      return { variant: 'standard' };
	    }
	  }]);

	  return Post;
	})(_react2['default'].Component);

	exports['default'] = Post;
	module.exports = exports['default'];

/***/ }

})