webpackHotUpdate(0,[
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(160);

	var _reactRouter2 = _interopRequireDefault(_reactRouter);

	var _componentsApp = __webpack_require__(367);

	var _componentsApp2 = _interopRequireDefault(_componentsApp);

	var _componentsPosts = __webpack_require__(!(function webpackMissingModule() { var e = new Error("Cannot find module \"./components/Posts\""); e.code = 'MODULE_NOT_FOUND'; throw e; }()));

	var _componentsPosts2 = _interopRequireDefault(_componentsPosts);

	var _componentsSettings = __webpack_require__(374);

	var _componentsSettings2 = _interopRequireDefault(_componentsSettings);

	__webpack_require__(376);

	var Route = _reactRouter2['default'].Route;
	var Redirect = _reactRouter2['default'].Redirect;

	var routes = _react2['default'].createElement(
	  Route,
	  {
	    name: 'app',
	    path: '/',
	    handler: _componentsApp2['default'] },
	  _react2['default'].createElement(Route, {
	    name: 'posts',
	    path: '/posts',
	    handler: _componentsPosts2['default'] }),
	  _react2['default'].createElement(
	    Route,
	    {
	      name: 'imagePosts',
	      path: '/posts/images',
	      handler: _componentsPosts2['default'] },
	    _react2['default'].createElement(Route, {
	      name: 'imagePostDetail',
	      path: '/posts/images/:id',
	      handler: _componentsPosts2['default'] })
	  ),
	  _react2['default'].createElement(
	    Route,
	    {
	      name: 'videoPosts',
	      path: '/posts/videos',
	      handler: _componentsPosts2['default'] },
	    _react2['default'].createElement(Route, {
	      name: 'videoPostDetail',
	      path: '/posts/videos/:id',
	      handler: _componentsPosts2['default'] })
	  ),
	  _react2['default'].createElement(Route, {
	    name: 'settings',
	    path: '/settings',
	    handler: _componentsSettings2['default'] }),
	  _react2['default'].createElement(Redirect, { from: '/', to: 'posts' })
	);

	_reactRouter2['default'].run(routes, function (Handler) {
	  _react2['default'].render(_react2['default'].createElement(Handler, null), document.getElementById('react-app'));
	});

/***/ }
])