import React from 'react';
import _ from 'lodash';
import classnames from 'classnames';

import Post from './Post';

export default class Comments extends React.Component {

  constructor(props) {
    super(props);
    this.state = {expanded : false};
    this._toggleComments = this._toggleComments.bind(this);
  }

  static get propTypes() {
    return {
      comments : React.PropTypes.arrayOf(
        React.PropTypes.shape({
          user       : React.PropTypes.object,
          avatar     : React.PropTypes.string,
          comment    : React.PropTypes.string,
          createDate : React.PropTypes.string
        })
      )
    };
  }

  _toggleComments() {
    this.setState({expanded : !this.state.expanded});
  }

  render() {
    var linkClasses = classnames('comments-expand', {
      'comments-expand-open'   : this.state.expanded,
      'comments-expand-closed' : !this.state.expanded
    });

    if (_.isEmpty(this.props.comments)) {
      return null;
    }

    return (
      <div className="comments-wrap">
        <a
          onClick={this._toggleComments}
          className={linkClasses}>
          {this.state.expanded ? 'collapse' : 'expand'}
          <span className="comments-expand-triangle"></span>
        </a>
        {this.state.expanded &&
          <div className="comments">
            {_.map(this.props.comments, comment => {
              return (
                <Post
                  className="comment"
                  key={comment.id}
                  message={comment.comment}
                  avatar={comment.user.avatar}
                  username={comment.user.name}/>
              );
            })}
            <div className="comment-add">
              <input
                type="text"
                placeholder="Reply..." />
            </div>
          </div>
        }
      </div>
    );
  }
}