import React from 'react';
import classnames from 'classnames';
import _ from 'lodash';

export default class Modal extends React.Component {

  static get defaultProps() {
    return {'onClose' : _.noop};
  }

  render() {
    var classes = classnames('modal', this.props.className);

    return (
      <div className="modal-backdrop">
        <div className={classes}>
          <div
            className="modal-close"
            onClick={this.props.onClose}>
            <i className="fa fa-times-circle-o" />
          </div>
          <div className="modal-content">
            {this.props.title &&
              <h2 className="modal-title">{this.props.title}</h2>
            }
            <div>{this.props.children}</div>
          </div>
        </div>
      </div>
    );
  }
}

Modal.propTypes = {
  children : React.PropTypes.node.isRequired,
  onClose  : React.PropTypes.func,
  title    : React.PropTypes.node
};

