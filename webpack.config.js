var webpack = require('webpack');
var path = require('path');
var ExtractTextPlugin = require("extract-text-webpack-plugin");

module.exports = {
  context : __dirname + "/src",
  entry   : "./entry",
  module  : {
    loaders : [{
      test    : /\.scss$/,
      loaders : [
        ExtractTextPlugin.extract("style-loader"), 
        'css', 
        'autoprefixer', 
        'sass'
      ]
    }, {
      test : /\.es6\.js$/,
      exclude : '/node_modules/',
      loaders : ['babel']
    }, {
      test    : /\.jsx$/,
      exclude : /node_modules/,
      loaders : ['babel'] //?optional[]=runtime'
    }],
    preLoaders: [{
      test    : /(\.es6\.js|\.jsx)$/,
      exclude : 'node_modules',
      loaders : ['eslint']
    }]
  },
  resolve : {
    alias : {
      'styles' :   './sass/'
    },
    extensions : ['', '.webpack.js', '.web.js', '.js', '.jsx', '.es6.js']
  },
  output  : {
    path     : __dirname + "/dist/bundles/",
    filename : "js/[name].bundle.js"
  },
  plugins: [
    new ExtractTextPlugin("css/[name].css")
  ],
  eslint: {
    configFile: __dirname + '/.eslintrc'
  }
}