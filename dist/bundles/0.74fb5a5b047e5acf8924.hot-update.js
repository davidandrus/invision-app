webpackHotUpdate(0,{

/***/ 0:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(160);

	var _reactRouter2 = _interopRequireDefault(_reactRouter);

	var _componentsApp = __webpack_require__(367);

	var _componentsApp2 = _interopRequireDefault(_componentsApp);

	var _componentsPosts = __webpack_require__(1);

	var _componentsPosts2 = _interopRequireDefault(_componentsPosts);

	var _componentsSettings = __webpack_require__(374);

	var _componentsSettings2 = _interopRequireDefault(_componentsSettings);

	__webpack_require__(376);

	var Route = _reactRouter2['default'].Route;
	var Redirect = _reactRouter2['default'].Redirect;

	var routes = _react2['default'].createElement(
	  Route,
	  {
	    name: 'app',
	    path: '/',
	    handler: _componentsApp2['default'] },
	  _react2['default'].createElement(Route, {
	    name: 'posts',
	    path: '/posts',
	    handler: _componentsPosts2['default'] }),
	  _react2['default'].createElement(
	    Route,
	    {
	      name: 'imagePosts',
	      path: '/posts/images',
	      handler: _componentsPosts2['default'] },
	    _react2['default'].createElement(Route, {
	      name: 'imagePostDetail',
	      path: '/posts/images/:id',
	      handler: _componentsPosts2['default'] })
	  ),
	  _react2['default'].createElement(
	    Route,
	    {
	      name: 'videoPosts',
	      path: '/posts/videos',
	      handler: _componentsPosts2['default'] },
	    _react2['default'].createElement(Route, {
	      name: 'videoPostDetail',
	      path: '/posts/videos/:id',
	      handler: _componentsPosts2['default'] })
	  ),
	  _react2['default'].createElement(Route, {
	    name: 'settings',
	    path: '/settings',
	    handler: _componentsSettings2['default'] }),
	  _react2['default'].createElement(Redirect, { from: '/', to: 'posts' })
	);

	_reactRouter2['default'].run(routes, function (Handler) {
	  _react2['default'].render(_react2['default'].createElement(Handler, null), document.getElementById('react-app'));
	});

/***/ },

/***/ 1:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(160);

	var _reactRouter2 = _interopRequireDefault(_reactRouter);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _Post = __webpack_require__(201);

	var _Post2 = _interopRequireDefault(_Post);

	var _AddMedia = __webpack_require__(2);

	var _AddMedia2 = _interopRequireDefault(_AddMedia);

	var _Modal = __webpack_require__(288);

	var _Modal2 = _interopRequireDefault(_Modal);

	var State = _reactRouter2['default'].State;
	var Navigation = _reactRouter2['default'].Navigation;

	// should try not to use create class here
	var PostsList = _react2['default'].createClass({
	  displayName: 'PostsList',

	  mixins: [State, Navigation],
	  render: function render() {
	    var _this = this;

	    var getLinkClass = function getLinkClass(path) {
	      return _this.isActive(path) ? 'selected' : '';
	    };

	    var posts;
	    var backPath;
	    var imagePathName = 'imagePosts';
	    var videoPathName = 'videoPosts';
	    var isImagePost = _lodash2['default'].some(this.getRoutes(), { name: imagePathName });
	    var isVideoPost = _lodash2['default'].some(this.getRoutes(), { name: videoPathName });

	    if (isImagePost) {
	      backPath = imagePathName;
	      posts = _lodash2['default'].filter(this.props.posts, { type: 'image' });
	    } else if (isVideoPost) {
	      backPath = videoPathName;
	      posts = _lodash2['default'].filter(this.props.posts, { type: 'video' });
	    } else {
	      backPath = 'posts';
	      posts = this.props.posts;
	    }

	    var getPostModal = function getPostModal() {
	      var post = _lodash2['default'].find(posts, { id: parseInt(_this.getParams().id, 10) });

	      if (!post) {
	        return null;
	      }

	      return _react2['default'].createElement(
	        _Modal2['default'],
	        {
	          className: 'post-detail-modal',
	          close: function () {
	            return _this.transitionTo(backPath);
	          } },
	        _react2['default'].createElement(_Post2['default'], {
	          variant: 'modal',
	          createDate: post.createDate,
	          type: post.type,
	          image: post.image,
	          key: post.id,
	          message: post.text,
	          avatar: post.user.avatar,
	          username: post.user.name,
	          comments: post.comments })
	      );
	    };

	    return _react2['default'].createElement(
	      'div',
	      { className: 'posts-wrapper' },
	      _react2['default'].createElement(
	        'section',
	        { className: 'post-entry' },
	        _react2['default'].createElement(
	          'div',
	          { className: 'post-entry-inner' },
	          _react2['default'].createElement('input', {
	            type: 'text',
	            className: 'post-entry-input',
	            placeholder: 'what\'s on your mind?' }),
	          _react2['default'].createElement(_AddMedia2['default'], { className: 'post-entry-add-media' })
	        )
	      ),
	      _react2['default'].createElement(
	        'nav',
	        { className: 'main-nav' },
	        _react2['default'].createElement(
	          'ul',
	          { className: 'tabs' },
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('posts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts' },
	              'All Posts'
	            )
	          ),
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('imagePosts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts/images' },
	              'Photos'
	            )
	          ),
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('videoPosts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts/videos' },
	              'Videos'
	            )
	          )
	        ),
	        _react2['default'].createElement(
	          'ul',
	          { className: 'layouts' },
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('a', { title: 'list view' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('a', { title: 'grid view' })
	          )
	        )
	      ),
	      _react2['default'].createElement(
	        'div',
	        { className: 'posts' },
	        _lodash2['default'].map(_lodash2['default'].take(posts, 20), function (post) {
	          return _react2['default'].createElement(_Post2['default'], {
	            id: post.id,
	            createDate: post.createDate,
	            type: post.type,
	            image: post.image,
	            key: post.id,
	            message: post.text,
	            avatar: post.user.avatar,
	            username: post.user.name,
	            comments: post.comments });
	        })
	      ),
	      getPostModal()
	    );
	  }
	});

	var Posts = (function (_React$Component) {
	  function Posts() {
	    _classCallCheck(this, Posts);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Posts, _React$Component);

	  _createClass(Posts, [{
	    key: 'render',
	    value: function render() {
	      return _react2['default'].createElement(PostsList, { posts: posts });
	    }
	  }]);

	  return Posts;
	})(_react2['default'].Component);

	exports['default'] = Posts;
	module.exports = exports['default'];

/***/ },

/***/ 201:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _moment = __webpack_require__(202);

	var _moment2 = _interopRequireDefault(_moment);

	var _classnames = __webpack_require__(159);

	var _classnames2 = _interopRequireDefault(_classnames);

	var _Comments = __webpack_require__(287);

	var _Comments2 = _interopRequireDefault(_Comments);

	var Post = (function (_React$Component) {
	  function Post() {
	    _classCallCheck(this, Post);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Post, _React$Component);

	  _createClass(Post, [{
	    key: 'render',
	    value: function render() {
	      var classes = (0, _classnames2['default'])('post', this.props.className, {
	        'post-no-media': !!this.props.image
	      });
	      var variant = this.props.variant;

	      if (this.props.image) {
	        var imageComponentClasses = (0, _classnames2['default'])('post-media', 'post-media-' + variant);
	        var href = variant !== 'modal' ? '#/posts/' + this.props.type + 's/' + this.props.id : '#';

	        var imageComponent = _react2['default'].createElement(
	          'div',
	          { className: imageComponentClasses },
	          _react2['default'].createElement(
	            'a',
	            { href: href },
	            _react2['default'].createElement('img', { src: this.props.image.md })
	          ),
	          this.props.type === 'video' && _react2['default'].createElement(
	            'div',
	            { className: 'post-video-indicator' },
	            _react2['default'].createElement('i', { className: 'fa fa-play-circle-o' })
	          )
	        );

	        var normalPostImage = variant === 'standard' && imageComponent;
	        var modalPostImage = variant === 'modal' && imageComponent;
	      }

	      return _react2['default'].createElement(
	        'div',
	        { className: classes },
	        modalPostImage,
	        _react2['default'].createElement(
	          'div',
	          { className: 'post-wrap' },
	          _react2['default'].createElement(
	            'div',
	            { className: 'post-username' },
	            this.props.username
	          ),
	          _react2['default'].createElement(
	            'div',
	            { className: 'post-message' },
	            this.props.message
	          ),
	          _react2['default'].createElement('img', {
	            src: this.props.avatar,
	            className: 'post-avatar',
	            height: '40',
	            width: '40' })
	        ),
	        _react2['default'].createElement(
	          'ul',
	          { className: 'post-meta' },
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('i', { className: 'fa fa-heart-o' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('i', { className: 'fa fa-reply' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            (0, _moment2['default'])(this.props.createDate).fromNow(true)
	          )
	        ),
	        normalPostImage,
	        !_lodash2['default'].isEmpty(this.props.comments) && _react2['default'].createElement(_Comments2['default'], { comments: this.props.comments })
	      );
	    }
	  }], [{
	    key: 'propTypes',
	    get: function () {
	      return {
	        username: _react2['default'].PropTypes.string,
	        message: _react2['default'].PropTypes.string,
	        image: _react2['default'].PropTypes.object,
	        avatar: _react2['default'].PropTypes.string,
	        createDate: _react2['default'].PropTypes.string,
	        comments: _react2['default'].PropTypes.array,
	        variant: _react2['default'].PropTypes.string
	      };
	    }
	  }]);

	  return Post;
	})(_react2['default'].Component);

	exports['default'] = Post;

	Post.defaultProps = {
	  variant: 'standard'
	};
	module.exports = exports['default'];

/***/ },

/***/ 287:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _moment = __webpack_require__(202);

	var _moment2 = _interopRequireDefault(_moment);

	var _Post = __webpack_require__(201);

	var _Post2 = _interopRequireDefault(_Post);

	var Comments = (function (_React$Component) {
	  function Comments() {
	    _classCallCheck(this, Comments);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Comments, _React$Component);

	  _createClass(Comments, [{
	    key: 'render',
	    value: function render() {
	      return _react2['default'].createElement(
	        'div',
	        { className: 'comments' },
	        _lodash2['default'].map(this.props.comments, function (comment) {
	          return _react2['default'].createElement(_Post2['default'], {
	            className: 'comment',
	            key: comment.id,
	            message: comment.comment,
	            avatar: comment.user.avatar,
	            username: comment.user.name });
	        }),
	        _react2['default'].createElement(
	          'div',
	          { className: 'comment-add' },
	          _react2['default'].createElement('input', {
	            type: 'text',
	            placeholder: 'Reply...' })
	        )
	      );
	    }
	  }], [{
	    key: 'propTypes',
	    get: function () {
	      return {
	        comments: _react2['default'].PropTypes.arrayOf(_react2['default'].PropTypes.shape({
	          user: _react2['default'].PropTypes.object,
	          avatar: _react2['default'].PropTypes.string,
	          comment: _react2['default'].PropTypes.string,
	          createDate: _react2['default'].PropTypes.string
	        }))
	      };
	    }
	  }]);

	  return Comments;
	})(_react2['default'].Component);

	exports['default'] = Comments;
	module.exports = exports['default'];

/***/ },

/***/ 288:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _classnames = __webpack_require__(159);

	var _classnames2 = _interopRequireDefault(_classnames);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var Modal = (function (_React$Component) {
	  function Modal() {
	    _classCallCheck(this, Modal);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Modal, _React$Component);

	  _createClass(Modal, [{
	    key: 'render',
	    value: function render() {
	      var classes = (0, _classnames2['default'])('modal', this.props.className);

	      return _react2['default'].createElement(
	        'div',
	        { className: 'modal-backdrop' },
	        _react2['default'].createElement(
	          'div',
	          { className: classes },
	          _react2['default'].createElement(
	            'div',
	            {
	              className: 'modal-close',
	              onClick: this.props.close },
	            _react2['default'].createElement('i', { className: 'fa fa-times-circle-o' })
	          ),
	          _react2['default'].createElement(
	            'div',
	            { className: 'modal-content' },
	            this.props.title && _react2['default'].createElement(
	              'h2',
	              { className: 'modal-title' },
	              this.props.title
	            ),
	            _react2['default'].createElement(
	              'div',
	              null,
	              this.props.children
	            )
	          )
	        )
	      );
	    }
	  }], [{
	    key: 'defaultProps',
	    get: function () {
	      return { 'close': _lodash2['default'].noop };
	    }
	  }]);

	  return Modal;
	})(_react2['default'].Component);

	exports['default'] = Modal;

	Modal.propTypes = {
	  children: _react2['default'].PropTypes.node.isRequired,
	  close: _react2['default'].PropTypes.func,
	  title: _react2['default'].PropTypes.node
	};
	module.exports = exports['default'];

/***/ }

})