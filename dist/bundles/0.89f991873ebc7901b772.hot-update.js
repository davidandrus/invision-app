webpackHotUpdate(0,{

/***/ 370:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _flummox = __webpack_require__(310);

	var PostsActions = (function (_Actions) {
	  function PostsActions() {
	    _classCallCheck(this, PostsActions);

	    if (_Actions != null) {
	      _Actions.apply(this, arguments);
	    }
	  }

	  _inherits(PostsActions, _Actions);

	  _createClass(PostsActions, [{
	    key: 'filterPosts',
	    value: function filterPosts(filter) {
	      return { filter: filter };
	    }
	  }]);

	  return PostsActions;
	})(_flummox.Actions);

	exports.PostsActions = PostsActions;

/***/ }

})