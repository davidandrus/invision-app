webpackHotUpdate(0,{

/***/ 287:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _Post = __webpack_require__(201);

	var _Post2 = _interopRequireDefault(_Post);

	var Comments = (function (_React$Component) {
	  function Comments() {
	    _classCallCheck(this, Comments);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Comments, _React$Component);

	  _createClass(Comments, [{
	    key: 'render',
	    value: function render() {
	      return _react2['default'].createElement(
	        'div',
	        { className: 'comments' },
	        _lodash2['default'].map(this.props.comments, function (comment) {
	          return _react2['default'].createElement(_Post2['default'], {
	            className: 'comment',
	            key: comment.id,
	            message: comment.comment,
	            avatar: comment.user.avatar,
	            username: comment.user.name });
	        }),
	        _react2['default'].createElement(
	          'div',
	          { className: 'comment-add' },
	          _react2['default'].createElement('input', {
	            type: 'text',
	            placeholder: 'Reply...' })
	        )
	      );
	    }
	  }], [{
	    key: 'propTypes',
	    get: function () {
	      return {
	        comments: _react2['default'].PropTypes.arrayOf(_react2['default'].PropTypes.shape({
	          user: _react2['default'].PropTypes.object,
	          avatar: _react2['default'].PropTypes.string,
	          comment: _react2['default'].PropTypes.string,
	          createDate: _react2['default'].PropTypes.string
	        }))
	      };
	    }
	  }]);

	  return Comments;
	})(_react2['default'].Component);

	exports['default'] = Comments;
	module.exports = exports['default'];

/***/ }

})