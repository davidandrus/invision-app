import React from 'react';
import Router from 'react-router';
import App from './components/App';
import Posts from './components/Posts';
import Settings from './components/Settings';

require('styles/main.scss');

var Route = Router.Route;
var Redirect = Router.Redirect;

var routes = (
  <Route
    name="app"
    path="/"
    handler={App}>

    <Route
      name="posts"
      path="/posts"
      handler={Posts} />
    <Route
      name="imagePosts"
      path="/posts/images"
      handler={Posts}>
      <Route
        name="imagePostDetail"
        path="/posts/images/:id"
        handler={Posts} />
    </Route>

    <Route
      name="videoPosts"
      path="/posts/videos"
      handler={Posts}>
      <Route
        name="videoPostDetail"
        path="/posts/videos/:id"
        handler={Posts} />
    </Route>
    <Route
      name="settings"
      path="/settings"
      handler={Settings} />

    <Redirect from="/" to="posts"/>
  </Route>
);

Router.run(routes, function (Handler) {
  React.render(<Handler/>, document.getElementById('react-app'));
});
