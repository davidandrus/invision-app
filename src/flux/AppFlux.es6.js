import { Flux } from 'flummox';
import { PostsStore } from './PostsStore';
import { PostsActions } from './PostsActions';

export default class AppFlux extends Flux {

  constructor() {
    super();
    this.createActions('posts', PostsActions);

    // The extra argument(s) are passed to the MessageStore constructor
    this.createStore('posts', PostsStore, this);
  }

}