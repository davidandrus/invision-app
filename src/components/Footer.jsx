import React from 'react';

export default class Footer extends React.Component {
  render() {
    return (
      <footer className="footer">
        <ul className="footer-links">
          <li><a href="#">About Us</a></li>
          <li><a href="#">Support</a></li>
          <li><a href="#">Privacy</a></li>
          <li><a href="#">Terms</a></li>
          <li className="fooer-copyright">&copy; 2014 SimplySocial</li>
        </ul>
      </footer>
    );
  }
}

