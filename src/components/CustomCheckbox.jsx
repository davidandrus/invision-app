import React from 'react';
import classnames from 'classnames';

export default class CustomCheckbox extends React.Component {

  static get propTypes() {
    return {
      checked : React.PropTypes.bool
    };
  }

  render() {
    var classes = classnames('custom-checkbox', {
      'custom-checkbox-on'  : this.props.checked,
      'custom-checkbox-off' : !this.props.checked
    });

    return (
      <div className={classes}>
        <div className="custom-checkbox-toggle" />
      </div>
    );
  }
}

