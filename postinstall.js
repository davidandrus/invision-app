if (process.env.NODE_ENV != 'production') {
  console.log('postinstall running');
  var fs = require('fs-extra');

  // move vendor files from node modules and rename the to _{name}.scss files so that sass can use them
  fs.copySync(
   'node_modules/normalize.css/normalize.css',
   'src/sass/_normalize.scss'
  );
}