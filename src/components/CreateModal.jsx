import React from 'react';
import AddMedia from './AddMedia';

export default class CreateModal extends React.Component {

  render() {
    return (
      <div>
        <div className="modal">
          <button className="modal-close">X</button>
          <div className="modal-content">
            <h2 className="modal-title">Create New Message</h2>
            <textarea />
            <AddMedia />
            <button className="modal-post">Post</button>
          </div>
        </div>
        <div className="modal-backdrop"></div>
      </div>
    );
  }
}