webpackHotUpdate(0,{

/***/ 367:
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ('value' in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _classnames = __webpack_require__(159);

	var _classnames2 = _interopRequireDefault(_classnames);

	var _reactRouter = __webpack_require__(160);

	var _reactRouter2 = _interopRequireDefault(_reactRouter);

	var _moment = __webpack_require__(202);

	var _moment2 = _interopRequireDefault(_moment);

	var _flummoxComponent = __webpack_require__(289);

	var _flummoxComponent2 = _interopRequireDefault(_flummoxComponent);

	var _fluxAppFlux = __webpack_require__(368);

	var _fluxAppFlux2 = _interopRequireDefault(_fluxAppFlux);

	var _Header = __webpack_require__(371);

	var _Header2 = _interopRequireDefault(_Header);

	var _Footer = __webpack_require__(372);

	var _Footer2 = _interopRequireDefault(_Footer);

	var RouteHandler = _reactRouter2['default'].RouteHandler;
	var flux = new _fluxAppFlux2['default']();

	// setup realtive dates for moment
	_moment2['default'].locale('en', {
	  relativeTime: {
	    future: 'in %s',
	    past: '%s ago',
	    s: '1s',
	    m: '1m',
	    mm: '%dm',
	    h: '1h',
	    hh: '%dh',
	    d: '1d',
	    dd: '%dd',
	    M: '1m',
	    MM: '%dm',
	    y: '1y',
	    yy: '%dy'
	  }
	});

	var App = (function (_React$Component) {
	  function App() {
	    _classCallCheck(this, App);

	    _get(Object.getPrototypeOf(App.prototype), 'constructor', this).call(this);
	    this._setMediaQueries = this._setMediaQueries.bind(this);
	  }

	  _inherits(App, _React$Component);

	  _createClass(App, [{
	    key: 'componentWillMount',
	    value: function componentWillMount() {
	      this._setMediaQueries();
	      this.resize = this._setMediaQueries;
	      window.addEventListener('resize', this.resize);
	    }
	  }, {
	    key: 'componentWillUnmount',
	    value: function componentWillUnmount() {
	      window.removeEventListener('resize', this.resize);
	    }
	  }, {
	    key: 'shouldComponentUpdate',
	    value: function shouldComponentUpdate(nextState) {
	      return nextState.containerClassses !== this.state.containerClasses;
	    }
	  }, {
	    key: '_setMediaQueries',
	    value: function _setMediaQueries() {
	      var width = window.innerWidth;
	      var classes = (0, _classnames2['default'])('main-container', /*'modal-open',*/{
	        'mq-medium': width > 580
	      });

	      this.setState({ containerClasses: classes });
	    }
	  }, {
	    key: 'render',
	    value: function render() {
	      return _react2['default'].createElement(
	        'div',
	        { className: this.state.containerClasses },
	        _react2['default'].createElement(
	          'div',
	          { className: 'content-container' },
	          _react2['default'].createElement(_Header2['default'], null),
	          _react2['default'].createElement(
	            'main',
	            { className: 'main' },
	            _react2['default'].createElement(
	              _flummoxComponent2['default'],
	              { flux: flux },
	              _react2['default'].createElement(RouteHandler, null)
	            )
	          ),
	          _react2['default'].createElement(_Footer2['default'], null)
	        )
	      );
	    }
	  }]);

	  return App;
	})(_react2['default'].Component);

	exports['default'] = App;
	module.exports = exports['default'];

/***/ }

})