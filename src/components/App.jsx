import React from 'react';
import classnames from 'classnames';
import Router from 'react-router';
import moment from 'moment';

import FluxComponent from 'flummox/component';

import AppFlux from './../flux/AppFlux';
import Header from './Header';
import Footer from './Footer';
import Modal from './Modal';
import AddMedia from './AddMedia';

var RouteHandler = Router.RouteHandler;
var flux = new AppFlux();

// setup realtive dates for moment
moment.locale('en', {
  relativeTime : {
    future : 'in %s',
    past   : '%s ago',
    s      : '1s',
    m      : '1m',
    mm     : '%dm',
    h      : '1h',
    hh     : '%dh',
    d      : '1d',
    dd     : '%dd',
    M      : '1m',
    MM     : '%dm',
    y      : '1y',
    yy     : '%dy'
  }
});

export default class App extends React.Component {

  constructor() {
    super();
    this._setMediaQueries = this._setMediaQueries.bind(this);
    this._toggleCreateModal = this._toggleCreateModal.bind(this);
    this.state = {showCreateModal : false};
  }

  componentWillMount() {
    this._setMediaQueries();
    this.resize = this._setMediaQueries;
    window.addEventListener('resize', this.resize);
  }

  componentWillUnmount () {
    window.removeEventListener('resize', this.resize);
  }

  shouldComponentUpdate(nextState) {
    return nextState.containerClassses !== this.state.containerClasses;
  }

  _toggleCreateModal() {
    this.setState({showCreateModal : !this.state.showCreateModal});
  }

  _setMediaQueries() {
    var width = window.innerWidth;
    var classes = classnames('main-container', /*'modal-open',*/ {
      'mq-medium' : width > 580
    });

    this.setState({containerClasses : classes});
  }

  render() {
    return (
      <div className={this.state.containerClasses}>
        <div className="content-container">
          <Header onChatClick={this._toggleCreateModal}/>
          <main className="main">
            <FluxComponent flux={flux}>
              <RouteHandler />
            </FluxComponent>
          </main>
          <Footer />
        </div>
        {this.state.showCreateModal &&
          <Modal
            onClose={() => this.setState({showCreateModal : false})}
            className="create-modal"
            title="Create New Message">
            <textarea />
            <AddMedia />
            <button className="modal-post">Post</button>
          </Modal>
        }
      </div>
    );
  }
}