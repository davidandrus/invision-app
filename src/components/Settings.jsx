import React from 'react';
import _ from 'lodash';
import CustomCheckbox from './CustomCheckbox';
import {user} from './../dummyData';

export default class Settings extends React.Component {

  _handleCheckBoxClick(key) {
    var checkedKeys = this.state.checkedKeys;

    if (_.contains(checkedKeys, key)) {
      checkedKeys = _.without(checkedKeys, key);
    } else {
      checkedKeys.push(key);
    }

    this.setState({checkedKeys : checkedKeys});
  }

  constructor(props) {
    super(props);
    this.state = {checkedKeys : ['favorites']};
    this._handleCheckBoxClick = this._handleCheckBoxClick.bind(this);
  }

  render() {

    var checkboxesNotifications = [{
      key     : 'favorites',
      label   : 'email me when my posts are marked as favorites',
      checked : _.contains(this.state.checkedKeys, 'favorites')
    }, {
      key     : 'mentioned',
      label   : 'email me when I\'m mentioned',
      checked : _.contains(this.state.checkedKeys, 'mentioned')
    }, {
      key     : 'reply',
      label   : 'email me when I get a reply',
      checked : _.contains(this.state.checkedKeys, 'reply')
    }, {
      key     : 'follows',
      label   : 'email me when someone follows me',
      checked : _.contains(this.state.checkedKeys, 'follows')
    }];

    var checkboxesPrivacy = [{
      key     : 'location',
      label   : 'Add a location to my posts',
      checked : _.contains(this.state.checkedKeys, 'location')
    }, {
      key     : 'findByEmail',
      label   : 'let others find me by my email address',
      checked : _.contains(this.state.checkedKeys, 'findByEmail')
    }, {
      key     : 'tailor',
      label   : 'tailor ads based on my information',
      checked : _.contains(this.state.checkedKeys, 'tailor')
    }]

    var getCheckbox = checkbox => {
      return (
        <label
          key={checkbox.key}
          className="settings-checkbox"
          onClick={() => this._handleCheckBoxClick(checkbox.key)}>
          <span className="settings-checkbox-wrap">
            <CustomCheckbox
              checked={checkbox.checked} />
          </span>
          <span className="settings-checkbox-label">{checkbox.label}</span>
        </label>
      )
    }

    var getRadio = radio => {
      return (
        <label
          key={radioButton.key}
          className="settings-radioButton"
          onClick={() => this._handleRadioButtonClick(radioButton.key)}>
          <span className="settings-radioButton-wrap">
            <CustomRadioButton
              checked={radioButton.checked} />
          </span>
          <span className="settings-radioButton-label">{radioButton.label}</span>
        </label>
      )
    }

    return (
      <div className="standard-page settings">
        <h1 className="page-title">Settings</h1>
        <section>
          <h2>Account</h2>
          <div className="settings-personal">
            <div className="settings-avatar">
              <img src={user.avatar} className="settings-avatar-image" />
              <button className="settings-avatar-button button-alt">Change</button>
            </div>
            <form className="settings-text-entry">
              <div className="settings-username settings-input">
                <i className="settings-input-icon fa fa-user" />
                <input type="text" />
              </div>
              <div className="settings-email settings-input">
                <i className="settings-input-icon fa fa-envelope" />
                <input type="email" />
              </div>
              <div className="settings-password settings-input">
                <i className="settings-input-icon fa fa-lock" />
                <input type="password" />
              </div>
            </form>
          </div>
        </section>
        <section>
          <h2>Notifications</h2>
          {_.map(checkboxesNotifications, getCheckbox)}
        </section>
        {
        <section>
          <h2>Privacy</h2>
          {_.map(checkboxesPrivacy, getCheckbox)}
        </section>
        }
        <section className="form-actions">
          <button className="button-large">Save Changes</button>
        </section>
      </div>
    );
  }
}