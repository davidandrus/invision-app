import { Store } from 'flummox';
import { posts } from './../dummyData';
import _ from 'lodash';

export class PostsStore extends Store {

  constructor(flux) {
    super(flux);
    // const postActionIds = flux.getActionIds('posts');
    // this.register(postActionIds.filterPost, this.handlePostFilter);

    this.state = {posts : posts};
  }

  getFilteredPosts(type) {
    if (!type) {
      return this.state.posts;
    }
    return _.filter(this.state.posts, {'type' : type});
  }
}