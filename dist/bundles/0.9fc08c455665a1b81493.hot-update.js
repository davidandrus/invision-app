webpackHotUpdate(0,[
/* 0 */,
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	Object.defineProperty(exports, '__esModule', {
	  value: true
	});

	var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ('value' in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { 'default': obj }; }

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError('Cannot call a class as a function'); } }

	function _inherits(subClass, superClass) { if (typeof superClass !== 'function' && superClass !== null) { throw new TypeError('Super expression must either be null or a function, not ' + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) subClass.__proto__ = superClass; }

	var _react = __webpack_require__(3);

	var _react2 = _interopRequireDefault(_react);

	var _reactRouter = __webpack_require__(160);

	var _reactRouter2 = _interopRequireDefault(_reactRouter);

	var _lodash = __webpack_require__(199);

	var _lodash2 = _interopRequireDefault(_lodash);

	var _Post = __webpack_require__(201);

	var _Post2 = _interopRequireDefault(_Post);

	var _AddMedia = __webpack_require__(2);

	var _AddMedia2 = _interopRequireDefault(_AddMedia);

	var _Modal = __webpack_require__(288);

	var _Modal2 = _interopRequireDefault(_Modal);

	var _dummyData = __webpack_require__(319);

	var State = _reactRouter2['default'].State;
	var Navigation = _reactRouter2['default'].Navigation;

	// should try not to use create class here
	var PostsList = _react2['default'].createClass({
	  displayName: 'PostsList',

	  mixins: [State, Navigation],
	  render: function render() {
	    var _this = this;

	    var getLinkClass = function getLinkClass(path) {
	      return _this.isActive(path) ? 'selected' : '';
	    };

	    var posts;
	    var backPath;
	    var imagePathName = 'imagePosts';
	    var videoPathName = 'videoPosts';
	    var isImagePost = _lodash2['default'].some(this.getRoutes(), { name: imagePathName });
	    var isVideoPost = _lodash2['default'].some(this.getRoutes(), { name: videoPathName });

	    if (isImagePost) {
	      backPath = imagePathName;
	      posts = _lodash2['default'].filter(this.props.posts, { type: 'image' });
	    } else if (isVideoPost) {
	      backPath = videoPathName;
	      posts = _lodash2['default'].filter(this.props.posts, { type: 'video' });
	    } else {
	      backPath = 'posts';
	      posts = this.props.posts;
	    }

	    var getPostModal = function getPostModal() {
	      var post = _lodash2['default'].find(posts, { id: parseInt(_this.getParams().id, 10) });

	      if (!post) {
	        return null;
	      }

	      return _react2['default'].createElement(
	        _Modal2['default'],
	        {
	          className: 'post-detail-modal',
	          close: function () {
	            return _this.transitionTo(backPath);
	          } },
	        _react2['default'].createElement(_Post2['default'], {
	          variant: 'modal',
	          createDate: post.createDate,
	          type: post.type,
	          image: post.image,
	          key: post.id,
	          message: post.text,
	          avatar: post.user.avatar,
	          username: post.user.name,
	          comments: post.comments })
	      );
	    };

	    return _react2['default'].createElement(
	      'div',
	      { className: 'posts-wrapper' },
	      _react2['default'].createElement(
	        'section',
	        { className: 'post-entry' },
	        _react2['default'].createElement(
	          'div',
	          { className: 'post-entry-inner' },
	          _react2['default'].createElement('input', {
	            type: 'text',
	            className: 'post-entry-input',
	            placeholder: 'what\'s on your mind?' }),
	          _react2['default'].createElement(_AddMedia2['default'], { className: 'post-entry-add-media' })
	        )
	      ),
	      _react2['default'].createElement(
	        'nav',
	        { className: 'main-nav' },
	        _react2['default'].createElement(
	          'ul',
	          { className: 'tabs' },
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('posts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts' },
	              'All Posts'
	            )
	          ),
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('imagePosts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts/images' },
	              'Photos'
	            )
	          ),
	          _react2['default'].createElement(
	            'li',
	            { className: getLinkClass('videoPosts') },
	            _react2['default'].createElement(
	              'a',
	              { href: '#/posts/videos' },
	              'Videos'
	            )
	          )
	        ),
	        _react2['default'].createElement(
	          'ul',
	          { className: 'layouts' },
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('a', { title: 'list view' })
	          ),
	          _react2['default'].createElement(
	            'li',
	            null,
	            _react2['default'].createElement('a', { title: 'grid view' })
	          )
	        )
	      ),
	      _react2['default'].createElement(
	        'div',
	        { className: 'posts' },
	        _lodash2['default'].map(_lodash2['default'].take(posts, 20), function (post) {
	          return _react2['default'].createElement(_Post2['default'], {
	            id: post.id,
	            createDate: post.createDate,
	            type: post.type,
	            image: post.image,
	            key: post.id,
	            message: post.text,
	            avatar: post.user.avatar,
	            username: post.user.name,
	            comments: post.comments });
	        })
	      ),
	      getPostModal()
	    );
	  }
	});

	var Posts = (function (_React$Component) {
	  function Posts() {
	    _classCallCheck(this, Posts);

	    if (_React$Component != null) {
	      _React$Component.apply(this, arguments);
	    }
	  }

	  _inherits(Posts, _React$Component);

	  _createClass(Posts, [{
	    key: 'render',
	    value: function render() {
	      return _react2['default'].createElement(PostsList, { posts: _dummyData.Posts });
	    }
	  }]);

	  return Posts;
	})(_react2['default'].Component);

	exports['default'] = Posts;
	module.exports = exports['default'];

/***/ }
])