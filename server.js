var Hapi = require('hapi');
var server = new Hapi.Server();

server.connection({
  port : process.env.PORT || 3000
});

server.route({
  method: 'GET',
  path: '/{param*}',
  handler: {
    directory: {
      path: 'dist'
    }
  }
});

server.start(function () {
    console.log('Server started at [' + server.info.uri + ']');
});
