import React from 'react';
import classnames from 'classnames';

export default class AddMedia extends React.Component {
  render() {
    var classes = classnames('add-media', this.props.className);
    return (
      <ul className={classes}>
        <li><i className="fa fa-camera"></i>Add Photo</li>
        <li><i className="fa fa-video-camera"></i>Add Video</li>
      </ul>
    );
  }
}