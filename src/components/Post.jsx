import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import classnames from 'classnames';
import Comments from './Comments';

export default class Post extends React.Component {

  static get propTypes() {
    return {
      username   : React.PropTypes.string,
      message    : React.PropTypes.string,
      image      : React.PropTypes.object,
      avatar     : React.PropTypes.string,
      createDate : React.PropTypes.string,
      comments   : React.PropTypes.array,
      variant    : React.PropTypes.string
    };
  }

  static get defaultProps() {
    return {variant : 'standard'};
  }

  render() {
    var classes = classnames('post', this.props.className, {
      'post-no-media' : !!this.props.image
    });
    var variant = this.props.variant;

    if (this.props.image) {
      let imageComponentClasses = classnames('post-media', `post-media-${variant}`);
      let href = variant !== 'modal' ? `#/posts/${this.props.type}s/${this.props.id}` : '#';

      var imageComponent = (
        <div className={imageComponentClasses}>
          <a href={href}><img src={this.props.image.md} /></a>
          {this.props.type === 'video' &&
            <div className="post-video-indicator"><i className="fa fa-play-circle-o" /></div>
          }
        </div>
      );

      var normalPostImage = variant === 'standard' && imageComponent;
      var modalPostImage = variant === 'modal' && imageComponent;
    }

    return (
      <div className={classes}>
        {modalPostImage}
        <div className="post-wrap">
          <div className="post-username">{this.props.username}</div>
          <div className="post-message">{this.props.message}</div>
          <img
            src={this.props.avatar}
            className="post-avatar"
            height="40"
            width="40"/>
        </div>
        <ul className="post-meta">
          <li><i className="fa fa-heart-o" /></li>
          <li><i className="fa fa-reply" /></li>
          <li>{moment(this.props.createDate).fromNow(true)}</li>
        </ul>
        {normalPostImage}
        {!_.isEmpty(this.props.comments) &&
          <Comments comments={this.props.comments} />
        }
      </div>
    );
  }
}