var faker = require('faker');
var _ = require('lodash');
var moment = require('moment');
var usersLength = 20;
var postsLength = 100;
var maxPostAge = moment().subtract(3, 'days');

export var users = _.map(_.range(1, usersLength + 1), function(index) {
  return {
    id     : index,
    name   : faker.name.findName(),
    avatar : faker.image.avatar()
  };
});

var getRandomUser = function() {
  return users[_.random(0, usersLength - 1)];
};

var getRandomDate = function(startDate, endDate) {
  startDate = startDate || maxPostAge;
  endDate = endDate || moment();
  var rand = _.random(startDate.unix(), endDate.unix());
  return moment(rand * 1000);
};

var posts = _.map(_.range(1, postsLength + 1), function(index) {
  var postDate = getRandomDate();

  var getComments = function() {
    var randArr = _.range(0, _.random(0, 3));
    var comments = _.map(randArr, function() {
      return {
        id         : _.uniqueId(),
        comment    : faker.lorem.sentence(),
        createDate : getRandomDate(postDate, moment()).format(),
        user       : getRandomUser()
      };
    });
    return _.sortBy(comments, 'createDate').reverse();
  };

  var obj = {
    id         : index,
    user       : getRandomUser(),
    createDate : postDate.format(),
    text       : faker.lorem.sentence(),
    comments   : getComments()
  };

  var types = ['text', 'video', 'image'];
  var categories = ['nightlife', 'fashion', 'people', 'nature', 'city', 'food', 'animals'];

  var imageIndex = _.random(1, 10);
  var imageCategory = categories[_.random(0, categories.length - 1)];
  obj.type = types[_.random(0, types.length - 1) ];

  if (obj.type !== 'text') {

    obj.image = {
      sm : faker.image[imageCategory](375, 225) + '/' + imageIndex,
      md : faker.image[imageCategory](554, 316) + '/' + imageIndex,
      lg : faker.image[imageCategory](974, 583) + '/' + imageIndex
    };
  }

  return obj;
});

posts = _.sortBy(posts, 'createDate').reverse();

export var posts;

export var user = getRandomUser();