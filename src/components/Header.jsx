import React from 'react';
import _ from 'lodash';
import {user} from './../dummyData';

export default class Header extends React.Component {

  constructor() {
    super();
    this._toggleUserNav = this._toggleUserNav.bind(this);
    this._showUserNav = this._showUserNav.bind(this);
    this._hideUserNav = this._hideUserNav.bind(this);
    this._onChatClick = this._onChatClick.bind(this);

    this.state = {
      userNavOpen : false
    };
  }

  static get defaultProps() {
    return {onChatClick : _.noop};
  }

  _toggleUserNav() {
    this.setState({
      userNavOpen : !this.state.userNavOpen
    });
  }

  _showUserNav() {
    this.setState({
      userNavOpen : true
    });
  }

  _hideUserNav() {
    this.setState({
      userNavOpen : false
    });
  }

  _onChatClick() {
    this.props.onChatClick();
  }

  render() {

    return (
      <header
        className="header"
        onMouseLeave={this._hideUserNav}>
        <div className="header-inner-wrap">
          <h1 className="header-logo">
            <img
              src="/assets/logo.png"
              alt="Simply Social" />
          </h1>
          <div className="header-utility">
            <a
              className="header-chat"
              onClick={this._onChatClick}>
              <i className="fa fa-comments-o" />
            </a>
            <form className="header-search">
              <input type="search" />
              <i className="fa fa-search"></i>
            </form>
            <div
              className="header-user"
              onClick={this._toggleUserNav}
              onMouseEnter={this._showUserNav}>
              <div className="header-avatar">
                <img src={user.avatar} />
              </div>
              <div className="header-triangle"></div>
            </div>
          </div>
          {this.state.userNavOpen &&
            <ul className="header-user-dropdown">
              <li><a href="#">Profile</a></li>
              <li><a href="#">Followers</a></li>
              <li><a href="#">Following</a></li>
              <li><a href="#/settings">Settings</a></li>
            </ul>
          }
        </div>
      </header>
    );
  }
}

Header.propTypes = {onChatClick : React.PropTypes.func};

