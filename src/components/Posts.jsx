import React from 'react';
import Router from 'react-router';
import _ from 'lodash';

import Post from './Post';
import AddMedia from './AddMedia';
import Modal from './Modal';
import { posts as PostData }from './../dummyData';

const {State, Navigation} = Router;

// should try not to use create class here
var PostsList = React.createClass({
  mixins : [State, Navigation],
  render : function() {

    var getLinkClass = path => {
      return this.isActive(path) ? 'selected' : '';
    };

    var posts;
    var backPath;
    var imagePathName = 'imagePosts';
    var videoPathName = 'videoPosts';
    var isImagePost = _.some(this.getRoutes(), {name : imagePathName});
    var isVideoPost = _.some(this.getRoutes(), {name : videoPathName});

    if (isImagePost) {
      backPath = imagePathName;
      posts = _.filter(this.props.posts, {type : 'image'});
    } else if (isVideoPost) {
      backPath = videoPathName;
      posts = _.filter(this.props.posts, {type : 'video'});
    } else {
      backPath = 'posts';
      posts = this.props.posts;
    }

    var getPostModal = () => {
      var post = _.find(posts, {id : parseInt(this.getParams().id, 10)});

      if (!post) { return null; }

      return (
        <Modal
          className="post-detail-modal"
          onClose={() => this.transitionTo(backPath)}>
          <Post
            variant="modal"
            createDate={post.createDate}
            type={post.type}
            image={post.image}
            key={post.id}
            message={post.text}
            avatar={post.user.avatar}
            username={post.user.name}
            comments={post.comments} />
        </Modal>
      );
    };

    return (
      <div className="posts-wrapper">
        <section className="post-entry">
          <div className="post-entry-inner">
            <input
              type="text"
              className="post-entry-input"
              placeholder="what&apos;s on your mind?" />
            <AddMedia className="post-entry-add-media" />
          </div>
        </section>
        <nav className="main-nav">
          <ul className="tabs">
            <li className={getLinkClass('posts')}>
              <a href="#/posts">All Posts</a>
            </li>
            <li className={getLinkClass('imagePosts')}>
              <a href="#/posts/images">Photos</a>
            </li>
            <li className={getLinkClass('videoPosts')}>
              <a href="#/posts/videos">Videos</a>
            </li>
          </ul>
          <ul className="layouts">
            <li><a title="list view"></a></li>
            <li><a title="grid view"></a></li>
          </ul>
        </nav>
        <div className="posts">
          {_.map(_.take(posts, 20), post => {
            return (
              <Post
                id={post.id}
                createDate={post.createDate}
                type={post.type}
                image={post.image}
                key={post.id}
                message={post.text}
                avatar={post.user.avatar}
                username={post.user.name}
                comments={post.comments} />
            );
          })}
        </div>
        {getPostModal()}
      </div>
    );
  }
});

export default class Posts extends React.Component {
  render() {
    return (
      <PostsList posts={PostData} />
    );
  }
}